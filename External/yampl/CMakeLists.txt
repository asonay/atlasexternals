# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building YAMPL for ATLAS.
#

# The package's name:
atlas_subdir( yampl )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/yamplBuild )

# Set up the build of YAMPL:
ExternalProject_Add( yampl
   PREFIX ${CMAKE_BINARY_DIR}
   GIT_REPOSITORY https://github.com/vitillo/yampl.git
   GIT_TAG 46f7a48
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ./configure --prefix=${_buildDir}
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/
   ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
add_dependencies( Package_yampl yampl )

# Install YAMPL:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES cmake/Findyampl.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )
