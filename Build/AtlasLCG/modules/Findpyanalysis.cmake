# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  PYANALYSIS_BINARY_PATH
#  PYANALYSIS_PYTHON_PATH
#
# Can be steered by PYANALYSIS_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Dependencies:
find_package( PythonLibs )
find_package( PythonInterp )
find_package( kiwisolver )
find_package( cycler )

# If it was already found, let's be quiet:
if( PYANALYSIS_FOUND )
   set( pyanalysis_FIND_QUIETLY TRUE )
endif()

# Ignore system paths when an LCG release was set up:
if( PYANALYSIS_LCGROOT )
   set( _extraPyAnaArgs NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH )
endif()
lcg_system_ignore_path_setup()

# Find the binary path:
find_path( PYANALYSIS_BINARY_PATH isympy
   PATH_SUFFIXES bin PATHS ${PYANALYSIS_LCGROOT}
   ${_extraPyAnaArgs} )

# Find the python path:
find_path( PYANALYSIS_PYTHON_PATH 
   NAMES site.py numpy
   PATH_SUFFIXES lib/python2.7/site-packages
   PATHS ${PYANALYSIS_LCGROOT}
   ${_extraPyAnaArgs} )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( pyanalysis DEFAULT_MSG
   PYANALYSIS_PYTHON_PATH PYANALYSIS_BINARY_PATH
   PYTHONLIBS_FOUND PYTHONINTERP_FOUND )

# Set up the RPM dependency:
lcg_need_rpm( pyanalysis )

# Clean up:
if( _extraPyAnaArgs )
   unset( _extraPyAnaArgs )
endif()
