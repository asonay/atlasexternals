# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Module setting up pip from the LCG release for the runtime environment.
#
# Sets:
#   - PIP_EXECUTABLE
#   - PIP_BINARY_PATH
#   - PIP_PYTHON_PATH
#
# Can be steered by PIP_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# If pip was found already, let's be quiet:
if( PIP_FOUND )
   set( pip_FIND_QUIETLY TRUE )
endif()

# If an LCG release was set up, ignore the local paths in the search:
if( PIP_LCGROOT )
   set( _ignorePathBackup ${CMAKE_SYSTEM_IGNORE_PATH} )
   set( CMAKE_SYSTEM_IGNORE_PATH /usr/include /usr/bin /usr/lib /usr/lib32
      /usr/lib64 )
   set( _extraPipArgs NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH )
endif()
lcg_system_ignore_path_setup()

# Find the pip executable:
find_program( PIP_EXECUTABLE pip
   PATH_SUFFIXES bin PATHS ${PIP_LCGROOT}
   ${_extraPipArgs} )
mark_as_advanced( PIP_EXECUTABLE )
get_filename_component( PIP_BINARY_PATH ${PIP_EXECUTABLE} PATH )

# Find the python path:
find_path( PIP_PYTHON_PATH site.py
   PATH_SUFFIXES lib/python2.7/site-packages
   PATHS ${PIP_LCGROOT}
   ${_extraPipArgs} )
mark_as_advanced( PIP_PYTHON_PATH )

# Restore the environment:
set( CMAKE_SYSTEM_IGNORE_PATH ${_ignorePathBackup} )
unset( _ignorePathBackup )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( pip DEFAULT_MSG
   PIP_EXECUTABLE PIP_BINARY_PATH PIP_PYTHON_PATH )

# Set up the RPM dependency:
lcg_need_rpm( pip )

# Clean up:
if( PIP_LCGROOT )
   set( CMAKE_SYSTEM_IGNORE_PATH ${_ignorePathBackup} )
   unset( _ignorePathBackup )
   unset( _extraPipArgs )
endif()
