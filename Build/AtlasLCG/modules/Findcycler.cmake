# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#   CYCLER_PYTHON_PATH
#
# Can be steered by CYCLER_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Dependencies:
find_package( PythonLibs )
find_package( PythonInterp )

# If it was already found, let's be quiet:
if( CYCLER_FOUND )
   set( cycler_FIND_QUIETLY TRUE )
endif()

# Ignore system paths when an LCG release was set up:
if( CYCLER_LCGROOT )
   set( _extraClrArgs NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH )
endif()
lcg_system_ignore_path_setup()

# Find the python path:
find_path( CYCLER_PYTHON_PATH
   NAMES cycler.py cycler/__init__.py
   PATH_SUFFIXES lib/python2.7/site-packages
   PATHS ${CYCLER_LCGROOT}
   ${_extraClrArgs} )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( cycler DEFAULT_MSG
   CYCLER_PYTHON_PATH PYTHONLIBS_FOUND PYTHONINTERP_FOUND )

# Set up the RPM dependency:
lcg_need_rpm( cycler )

# Clean up:
if( _extraClrArgs )
   unset( _extraClrArgs )
endif()
